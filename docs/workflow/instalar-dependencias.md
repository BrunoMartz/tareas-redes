---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Instalar dependencias de desarrollo
authors:
- Andrés Leonardo Hernández Bermúdez
---

# Instalar dependencias de desarrollo

El contenido de este repositorio se convierte de archivos fuente escritos en Markdown hacia un sitio estático HTML que está hospedado en GitLab pages.
Esto es posible gracias al _software_ `mkdocs` que se puede ejecutar en el _pipeline_ de CI/CD de GitLab o de manera local utilizando el `Makefile` que viene incluido en el sitio.

El repositorio también tiene un archivo de configuración del _software_ `pre-commit` que nos ayuda a evitar los errores comunes cuando se envían archivos al repositorio de `git`.

## Instalación `git` y `tree`

Necesitas tener la herramienta `git` instalada para hacer uso de los repositorios.

```bash
# apt install git tree
```

!!! note
    - Si utilizas **macOS**, puedes instalar mediante [`brew install git`][brew-git]
    - Si utilizas Windows, puedes instalar via [`choco install git`][choco-git] o [`winget install git`][winget]

## Instalación de `make`

Es necesario instalar `make` para construir el sitio de manera local.

```bash
# apt install build-essential make
```

## Instalación de dependencias de Python

Los programas `pre-commit` y `mkdocs` están escritos en Python y son necesarias las dependencias de desarrollo para el manejador de paquetes de Python (`pip3`) para poder instalarlos en el sistema.

```bash
# apt install python3-dev python3-pip
```

Verifica que tengas `pip3` instalado y agrega una ruta para alterar la variable de entorno `PATH` para tu usuario.

```bash
$ which pip3
/usr/bin/pip3

$ echo 'PATH=${HOME}/.local/bin:${PATH}' >> ~/.bashrc
```

!!! warning
    Debes de realizar alguna de las siguientes acciones después de modificar el archivo `~/.bashrc`:

    - Ejecuta `source ~/.bashrc` en la terminal actual para incluir los cambios
    - Abre otra ventana de la terminal para que se lea el nuevo contenido del archivo `~/.bashrc`
    - Cierra y abre tu sesión para que reconozca los cambios
    - Reinicia tu máquina

## Instalación de `pre-commit`

Instala `pre-commit` en tu perfil de usuario.

```bash
$ pip3 install --user pre-commit
Collecting pre-commit
	...
Installing collected packages: ... pre-commit
	...
Successfully installed ... pre-commit-2.20.0 ...
```

Verifica que el _shell_ reconozca la ubicación de `pre-commit`.

```bash
$ which pre-commit
/home/tonejito/.local/bin/pre-commit
```

## Instalación de `mkdocs`

Se instala `mkdocs` para ejecutar el equivalente del _pipeline_ de CI/CD de manera local.

```bash
$ pip install --user --requirement requirements.txt
Collecting mkdocs>=1.1.2
	...
Installing collected packages: ... mkdocs ...
	...
Successfully installed ... mkdocs-1.3.1 ...
```

Verifica que el _shell_ reconozca la ubicación de `mkdocs`.

```bash
$ which mkdocs
/home/tonejito/.local/bin/mkdocs
```

--------------------------------------------------------------------------------

!!! note
    - Continúa en [la siguiente página][siguiente] cuando tengas las herramientas instaladas.

--------------------------------------------------------------------------------

|                 ⇦           |        ⇧      |                  ⇨            |
|:----------------------------|:-------------:|------------------------------:|
|                             | [Arriba](../) | [Página siguiente][siguiente] |

[anterior]: ../../workflow
[arriba]: ../../workflow
[siguiente]: ../crear-fork

[brew]: https://brew.sh/#install
[brew-git]: https://formulae.brew.sh/formula/git#default
[choco]: https://chocolatey.org/install
[choco-git]: https://community.chocolatey.org/packages?q=git
[winget]: https://docs.microsoft.com/en-us/windows/package-manager/winget/
[winget-git]: https://github.com/microsoft/winget-pkgs/tree/master/manifests/g/Git/Git
